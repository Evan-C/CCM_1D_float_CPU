
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <string>

using namespace std;

typedef struct
{
	float **Data;
	float ***Sumtable;
}RfData;

RfData  rFData_1, rFData_2, sumTable_Double;

int **searchRes;
float **searchRes_sub;

int rf_size[2], roi_paras[6], search_paras[2], kernel_paras;

int searchWidth, searchHeight, paras_length;

void getconfig(int corr_s, int *rf_s, int *search_s, int *roi_s)
{
	kernel_paras = corr_s;
	rf_size[0] = rf_s[0];
	rf_size[1] = rf_s[1];
	search_paras[0] = search_s[0];
	search_paras[1] = search_s[1];
	for (int i = 0; i < 6; i++)
	{
		roi_paras[i] = roi_s[i];
	}
	roi_paras[1] += 1;
	roi_paras[3] += 1;
}


void SumCalculate_Single(float **rFData, float **out)
{
	for (int i = 0; i < rf_size[1]; i++)
	{
		out[i][0] = rFData[i][0];
		for (int j = 1; j < rf_size[0]; j++)
		{
			out[i][j] = powf(rFData[i][j], 2) + out[i][j - 1];
		}
	}
}



void createSumTable1D_numerator(float **rf1, float **rf2, float **out, int height, int width)
{
	for(int i=0;i<width;i++)
	{
		for(int j=0;j<height;j++)
		{
			if(j == 0)
			{
				out[i][j] = rf1[i][j] * rf2[i][j];
			}
			else
			{
				out[i][j] = rf1[i][j]*rf2[i][j] + out[i][j - 1];
			}
		}
	}
}

void SumCalculate_Double(float ***sumtable_d, float **s1, float **s2)
{
	int width = rf_size[1];
	int height = rf_size[0];

	float *shifted_a = (float*)calloc(paras_length,sizeof(int));

	for(int i = 0; i < paras_length + 1; i ++)
	{
		shifted_a[i] = i + search_paras[0];
	}

	float ***s2_s = (float ***)malloc(paras_length * sizeof(float **));
	for (int i = 0; i < paras_length; i++)
	{
		s2_s[i] = (float **)malloc(rf_size[1] * sizeof(float *));
		for (int j = 0; j < rf_size[1]; j++)
		{
			s2_s[i][j] = (float *)calloc(rf_size[0], sizeof(float));
		}
	}



	int AL = paras_length;

	for(int Ttime = 0;Ttime<AL;Ttime++)
	{
		int tao_y = shifted_a[Ttime];
		for(int i = 0; i<width;i++)
		{
			for(int j = 0;j<height - 1;j++)
			{
				if(1<=(j+ tao_y) && (j+tao_y)<=height)
				{
					s2_s[Ttime][i][j] = s2[i][j+tao_y];
				}
			}
		}

		createSumTable1D_numerator(s1, s2_s[Ttime], sumtable_d[Ttime], height, width);
	}
	free(shifted_a);
	for (int i = 0; i < paras_length; i++)
		{
			for (int j = 0; j < rf_size[1]; j++)
			{
				free(s2_s[i][j]);
			}
			free(s2_s[i]);
		}
        free(s2_s);
}



int FastNCC(float ***std, float *st1, float *st2, int corr_y, int corr_x)
{

	int half_ks = (int)(kernel_paras/2);
	int AL = paras_length;

	float RCC_temp = -10;
	int max_Pos = 0;

	float *ncc_coeff_array = (float*)calloc(AL+1,sizeof(float));
	int *shifted_a = (int*)calloc(AL, sizeof(int));
	for(int i = 0; i < AL; i ++)
	{
		shifted_a[i] = i + search_paras[0];
	}

	for(int eTime = 0; eTime < AL; eTime++)
	{
		int xPoint = eTime;
		float tNumerator = std[xPoint][corr_x][corr_y + half_ks] - std[xPoint][corr_x][corr_y - half_ks];
		float tDenominator_1 = st1[corr_y + half_ks] - st1[corr_y - half_ks];
		float tDenominator_2 = st2[shifted_a[xPoint] + corr_y + half_ks] - st2[shifted_a[xPoint] + corr_y - half_ks];


		float aDenominator = sqrt(tDenominator_1 * tDenominator_2);

		float txx = tNumerator * pow(aDenominator, -1);


		if(txx>RCC_temp)
		{
			RCC_temp = txx;
			max_Pos = shifted_a[eTime];
		}
	}
	return max_Pos;
}



extern "C" int* CCM1D_CPU_sumTable_float(float* RF1_s, float* RF2_s, int corr_s, int *rf_s, int *search_s, int *roi_s)
{

	getconfig(corr_s, rf_s, search_s, roi_s);

	int searchWidth = (roi_paras[3] - roi_paras[2]) / roi_paras[5];
	int searchHeight = ((roi_paras[1] - roi_paras[0]) / roi_paras[4]);
	paras_length = abs(search_paras[1]) + abs(search_paras[0]) + 1;


	rFData_1.Data = (float**)malloc(rf_size[1] * sizeof(float*));
	rFData_2.Data = (float**)malloc(rf_size[1] * sizeof(float*));
	for (int l = 0; l < rf_size[1]; ++l) {
		rFData_1.Data[l] = (float*)malloc(rf_size[0] * sizeof(float));
		rFData_2.Data[l] = (float*)malloc(rf_size[0] * sizeof(float));
	}

	for (int x = 0; x < rf_size[1]; x++)
	{
		for (int y = 0; y < rf_size[0]; y++)
		{
			rFData_1.Data[x][y] = RF1_s[x + y * rf_size[1]];
			rFData_2.Data[x][y] = RF2_s[x + y * rf_size[1]];
		}
	}

	int *searchRes_F = (int *)malloc(searchHeight * searchWidth * sizeof(int));

	rFData_1.Sumtable = (float ***)malloc(sizeof(float **));
	rFData_1.Sumtable[0] = (float **)malloc(rf_size[1] * sizeof(float *));
	rFData_2.Sumtable = (float ***)malloc(sizeof(float **));
	rFData_2.Sumtable[0] = (float **)malloc(rf_size[1] * sizeof(float *));
	for (int i = 0; i < rf_size[1]; i++)
	{
		rFData_1.Sumtable[0][i] = (float *)malloc(rf_size[0] * sizeof(float));
		rFData_2.Sumtable[0][i] = (float *)malloc(rf_size[0] * sizeof(float));
	}

	sumTable_Double.Sumtable = (float ***)malloc(paras_length * sizeof(float **));
	for (int i = 0; i < paras_length; i++)
	{
		sumTable_Double.Sumtable[i] = (float **)malloc(rf_size[1] * sizeof(float *));
		for (int j = 0; j < rf_size[1]; j++)
		{
			sumTable_Double.Sumtable[i][j] = (float *)malloc(rf_size[0] * sizeof(float));
		}
	}


	SumCalculate_Single(rFData_1.Data, rFData_1.Sumtable[0]);
	SumCalculate_Single(rFData_2.Data, rFData_2.Sumtable[0]);
	SumCalculate_Double(sumTable_Double.Sumtable, rFData_1.Data, rFData_2.Data);

	int x = 0;
	int y = 0;
	for (int i = roi_paras[2]; i < roi_paras[3]; i += roi_paras[5])
	{
		y = 0;
		for (int j = roi_paras[0]; j < roi_paras[1]; j += roi_paras[4])
		{
			searchRes_F[y + x * searchHeight] = FastNCC(sumTable_Double.Sumtable, rFData_1.Sumtable[0][i], rFData_2.Sumtable[0][i], j, i);
			y+=1;
		}

		x+=1;
	}

	for (int i = 0; i < paras_length; i++)
	{
		for (int j = 0; j < rf_size[1]; j++)
		{
			free(sumTable_Double.Sumtable[i][j]);
		}
		free(sumTable_Double.Sumtable[i]);
	}
	free(sumTable_Double.Sumtable);

	for (int i = 0; i < rf_size[1]; i++)
	{
		free(rFData_1.Sumtable[0][i]);
		free(rFData_2.Sumtable[0][i]);
	}
	free(rFData_2.Sumtable[0]);
	free(rFData_2.Sumtable);
	free(rFData_1.Sumtable[0]);
	free(rFData_1.Sumtable);

	for (int l = 0; l < rf_size[1]; ++l) {
		free(rFData_1.Data[l]);
		free(rFData_2.Data[l]);
	}
	free(rFData_1.Data);
	free(rFData_2.Data);

	return searchRes_F;
}



